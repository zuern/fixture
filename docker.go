// Copyright 2021 Kevin Zuern. All rights reserved.

package fixture

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/go-connections/nat"
)

var (
	// TimeoutImagePull maximum time to pull a docker image before failure.
	TimeoutImagePull = 3 * time.Minute

	// TimeoutFixtureStart maximum time to create and start a test fixture.
	TimeoutFixtureStart = 30 * time.Second

	// TimeoutCleanup maximum time to stop and clean up all test fixtures.
	TimeoutCleanup = 3 * time.Second
)

func pullDockerImage(image string) error {
	// Ensure image is pulled.
	ctx, cf := context.WithTimeout(context.Background(), TimeoutImagePull)
	LogFunc("Pulling docker image...\n")
	defer func(cf func()) { cf() }(cf)
	reader, err := dockerClient.ImagePull(ctx, image, types.ImagePullOptions{})
	if err != nil {
		err = fmt.Errorf("pull docker image failed: %s", err)
		return err
	}
	defer reader.Close()
	type progressDetail struct {
		Current float64
		Total   float64
	}
	type pullStatus struct {
		ID             string
		Status         string
		ProgressDetail *progressDetail
	}
	scan := bufio.NewScanner(reader)
	for scan.Scan() {
		LogFunc("                                                                           \r") // Clear the line if text already present.
		var status pullStatus
		if err = json.Unmarshal(scan.Bytes(), &status); err != nil {
			LogFunc(scan.Text() + "\n")
		} else {
			var b strings.Builder
			b.WriteString("   ")
			if status.ID != "" {
				b.WriteString(status.ID)
				b.WriteRune(' ')
			}
			b.WriteString(status.Status)
			if status.ProgressDetail != nil && status.ProgressDetail.Total != 0 {
				progress := status.ProgressDetail.Current / status.ProgressDetail.Total * 100
				b.WriteString(fmt.Sprintf(" (%.2f%%)", progress))
			}
			LogFunc(b.String() + "\r")
		}
	}
	LogFunc("\n")
	return nil
}

type startDockerOpts struct {
	Command      []string
	PortBindings nat.PortMap
}

func startDockerContainer(
	ctx context.Context,
	image string,
	opts ...startDockerOpts,
) (containerID, containerIP string, err error) {
	// Create container
	var command []string
	var portBindings nat.PortMap
	if len(opts) > 0 {
		command = opts[0].Command
		portBindings = opts[0].PortBindings
	}
	hostConfig := &container.HostConfig{PortBindings: portBindings}
	resp, err := dockerClient.ContainerCreate(ctx,
		&container.Config{
			Tty:   true,
			Cmd:   command,
			Image: image,
		},
		hostConfig, nil, nil, "")
	if err != nil {
		err = fmt.Errorf("create docker container failed: %s", err)
		return
	}
	containerID = resp.ID
	// Start container
	if err = dockerClient.ContainerStart(ctx, containerID, types.ContainerStartOptions{}); err != nil {
		err = fmt.Errorf("start docker container %s: %w", containerID, err)
		return containerID, containerIP, err
	}
	details, err := dockerClient.ContainerInspect(ctx, containerID)
	if err != nil {
		err = fmt.Errorf("get container details: %w", err)
	} else {
		for _, settings := range details.NetworkSettings.Networks {
			containerIP = settings.IPAddress
			break
		}
	}
	return containerID, containerIP, err
}

func stopDockerContainer(ctx context.Context, containerID string) error {
	err := dockerClient.ContainerRemove(ctx, containerID, types.ContainerRemoveOptions{
		RemoveVolumes: true,
		Force:         true,
	})
	if err != nil {
		err = fmt.Errorf("remove docker container: %w", err)
	}
	return err
}
