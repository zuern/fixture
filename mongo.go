// Copyright 2021 Kevin Zuern. All rights reserved.

package fixture

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/docker/go-connections/nat"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// EnvMongoURL env var read to set the mongo fixture URL.
const EnvMongoURL = "TEST_MONGO_URL"

// MongoVersion to be used when starting via Docker.
var MongoVersion = "latest"

// StartMongo returns a URL to a Mongo server. If TEST_MONGO_URL isn't set in
// the environment, this will attempt to start a Mongo server using docker. If
// Mongo cannot be started this function will panic.
func StartMongo() (mongoURL string) {
	if mongoURL = os.Getenv(EnvMongoURL); mongoURL != "" {
		logf("Using Mongo URL from environment: %s\n", mongoURL)
		return
	}
	if fixture := getFixture(EnvMongoURL); fixture != nil {
		mongoURL, _ = fixture.data.(string)
		LogFunc("Reusing existing Mongo fixture.\n")
		return
	}
	LogFunc("Attempting to start Mongo with docker.\n")
	image := fmt.Sprintf("mongo:%s", MongoVersion)
	pullDockerImage(image)
	ctx, cf := context.WithTimeout(context.Background(), TimeoutFixtureStart)
	defer func(cf func()) { cf() }(cf)
	hostPort := randomFreePort()
	opts := startDockerOpts{
		PortBindings: nat.PortMap{
			"27017/tcp": []nat.PortBinding{{
				HostPort: strconv.Itoa(hostPort),
			}},
		},
	}
	containerID, _, err := startDockerContainer(ctx, image, opts)
	if err != nil {
		panic(fmt.Sprintf("start mongo fixture: %s", err))
	}
	logf("Started Mongo in docker with container id %q\n", containerID)
	mongoURL = fmt.Sprintf("mongodb://localhost:%d", hostPort)
	if err = mongoWait(ctx, mongoURL); err != nil {
		panic(err.Error())
	}
	setFixture(&fixture{
		envVarName:        EnvMongoURL,
		dockerImage:       image,
		dockerContainerID: containerID,
		data:              mongoURL,
	})
	logf("Mongo fixture url: %q\n", mongoURL)
	return mongoURL
}

func mongoWait(ctx context.Context, mongoURL string) error {
	for ctx.Err() == nil {
		opts := (&options.ClientOptions{}).ApplyURI(mongoURL)
		mc, err := mongo.NewClient(opts)
		if err == nil {
			mc.Disconnect(ctx)
			return nil
		}
		time.Sleep(100 * time.Millisecond)
	}
	return fmt.Errorf("timed out waiting for mongo to be ready")
}
