// Copyright 2021 Kevin Zuern. All rights reserved.

package fixture_test

import (
	"context"
	"fmt"

	"github.com/nats-io/nats.go"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"zuern.dev/fixture"
)

// ExampleStartNats starts a NATS test fixture and connects with a nats client.
func ExampleStartNats() {
	// Silence fixture logging for the example.
	old := fixture.LogFunc
	defer func() { fixture.LogFunc = old }()
	fixture.LogFunc = func(_ string) {}

	// Start the test fixture and connect.
	natsURL := fixture.StartNats()
	client, err := nats.Connect(natsURL)
	if err == nil {
		client.Close()
	}

	// Call cleanup to tear down all test fixtures when done.
	fixture.Cleanup()

	fmt.Println(err)
	// Output: <nil>
}

func ExampleStartMongo() {
	// Silence fixture logging for the example.
	old := fixture.LogFunc
	defer func() { fixture.LogFunc = old }()
	fixture.LogFunc = func(_ string) {} // silence logs

	// Start the test fixture and connect.
	mongoURL := fixture.StartMongo()
	opts := (&options.ClientOptions{}).ApplyURI(mongoURL)
	client, err := mongo.Connect(context.TODO(), opts)
	if err == nil {
		client.Disconnect(context.TODO())
	}

	// Call cleanup to tear down all test fixtures when done.
	fixture.Cleanup()

	fmt.Println(err)
	// Output: <nil>
}
