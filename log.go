// Copyright 2021 Kevin Zuern. All rights reserved.

package fixture

import (
	"fmt"
	"testing"
)

// LogFunc is called to do any logging in the fixture package.
var LogFunc func(msg string) = func(msg string) {
	if testing.Verbose() {
		fmt.Print(msg)
	}
}

// logf log with fmt.Sprintf formatting.
func logf(format string, args ...interface{}) {
	LogFunc(fmt.Sprintf(format, args...))
}
