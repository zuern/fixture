// Copyright 2021 Kevin Zuern. All rights reserved.

package fixture_test

import (
	"os"
	"testing"

	"zuern.dev/fixture"
)

func TestMain(m *testing.M) {
	code := m.Run()
	fixture.Cleanup()
	os.Exit(code)
}
