// Copyright 2023 Kevin Zuern. All rights reserved.

package fixture

import (
	"net"
)

// randomFreePort returns a random available port on the system or panics.
func randomFreePort() int {
	listener, err := net.Listen("tcp", "localhost:0")
	if err != nil {
		panic(err)
	}
	addr := listener.Addr().(*net.TCPAddr)
	listener.Close()
	return addr.Port
}
