// Copyright 2021 Kevin Zuern. All rights reserved.

package fixture_test

import (
	"os"
	"strings"
	"testing"

	"github.com/nats-io/nats.go"
	"github.com/stretchr/testify/require"
	"zuern.dev/fixture"
)

func TestStartNATSEnv(t *testing.T) {
	const expect = "nats://foobarbaz:4222"
	os.Setenv(fixture.EnvNatsURL, expect)
	defer os.Unsetenv(fixture.EnvNatsURL)
	url := fixture.StartNats()
	require.Equal(t, expect, url)
	fixture.Cleanup()
}

func TestStartNATSDocker(t *testing.T) {
	require := require.New(t)
	// Ensure docker approach is used.
	os.Unsetenv(fixture.EnvNatsURL)

	url := fixture.StartNats()
	require.NotEmpty(url)
	require.True(strings.HasPrefix(url, "nats://"))
	u2 := fixture.StartNats()
	require.Equal(url, u2, "same fixture url should be returned")

	nc, err := nats.Connect(url)
	require.NoError(err, "should be able to connect to nats")
	nc.Close()

	fixture.Cleanup()
}
