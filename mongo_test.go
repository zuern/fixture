// Copyright 2021 Kevin Zuern. All rights reserved.

package fixture_test

import (
	"context"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"zuern.dev/fixture"
)

func TestStartMongoEnv(t *testing.T) {
	const expect = "mongodb://foobarbaz:27017"
	os.Setenv(fixture.EnvMongoURL, expect)
	defer os.Unsetenv(fixture.EnvMongoURL)
	url := fixture.StartMongo()
	require.Equal(t, expect, url)
	fixture.Cleanup()
}

func TestStartMongoDocker(t *testing.T) {
	require := require.New(t)
	// Ensure docker approach is used.
	os.Unsetenv(fixture.EnvMongoURL)

	url := fixture.StartMongo()
	require.NotEmpty(url)
	require.True(strings.HasPrefix(url, "mongodb://"))
	u2 := fixture.StartMongo()
	require.Equal(url, u2, "same fixture url should be returned")

	opts := (&options.ClientOptions{}).ApplyURI(url)
	mc, err := mongo.Connect(context.Background(), opts)
	require.NoError(err, "should be able to connect to mongo")

	ctx, cf := context.WithTimeout(context.Background(), 10*time.Second)
	err = mc.Ping(ctx, nil)
	cf()
	require.NoError(err, "should be able to ping mongo")
	mc.Disconnect(context.Background())

	fixture.Cleanup()
}
