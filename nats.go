// Copyright 2021 Kevin Zuern. All rights reserved.

package fixture

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/docker/go-connections/nat"
	"github.com/nats-io/nats.go"
)

// EnvNatsURL env var read to set the nats fixture URL.
const EnvNatsURL = "TEST_NATS_URL"

// NatsVersion to use when starting via Docker.
var NatsVersion = "latest"

// StartNats returns a URL to a NATS server. If TEST_NATS_URL isn't set in the
// environment, this will attempt to start a NATS server using docker. If NATS
// cannot be started this function will panic.
func StartNats() (natsURL string) {
	if natsURL = os.Getenv(EnvNatsURL); natsURL != "" {
		logf("Using NATS URL from environment: %s\n", natsURL)
		return
	}
	if fixture := getFixture(EnvNatsURL); fixture != nil {
		natsURL, _ = fixture.data.(string)
		LogFunc("Reusing existing NATS fixture.\n")
		return
	}
	LogFunc("Attempting to start NATS with docker.\n")
	image := "nats:" + NatsVersion
	pullDockerImage(image)
	ctx, cf := context.WithTimeout(context.Background(), TimeoutFixtureStart)
	defer func(cf func()) { cf() }(cf)
	hostPort := randomFreePort()
	opts := startDockerOpts{
		Command: []string{"-DV"},
		PortBindings: nat.PortMap{
			"4222/tcp": []nat.PortBinding{{
				HostPort: strconv.Itoa(hostPort),
			}},
			"6222/tcp": []nat.PortBinding{{
				HostPort: strconv.Itoa(randomFreePort()),
			}},
			"8222/tcp": []nat.PortBinding{{
				HostPort: strconv.Itoa(randomFreePort()),
			}},
		},
	}
	containerID, _, err := startDockerContainer(ctx, image, opts)
	if err != nil {
		panic(fmt.Sprintf("start nats fixture: %s", err))
	}
	logf("Started NATS in docker with container id %q\n", containerID)
	natsURL = fmt.Sprintf("nats://%s:%d", "localhost", hostPort)
	logf("NATS URL: %q\n", natsURL)
	if err = natsWait(ctx, natsURL); err != nil {
		panic(err.Error())
	}
	setFixture(&fixture{
		envVarName:        EnvNatsURL,
		dockerImage:       image,
		dockerContainerID: containerID,
		data:              natsURL,
	})
	logf("NATS fixture url: %q\n", natsURL)
	return natsURL
}

// natsWait waits until ctx expires or a connection to nats can be established.
// If timeout occurs an error is returned.
func natsWait(ctx context.Context, natsURL string) error {
	for ctx.Err() == nil {
		nc, err := nats.Connect(natsURL)
		if err == nil {
			nc.Close()
			return nil
		}
		time.Sleep(100 * time.Millisecond)
	}
	return fmt.Errorf("timed out waiting for NATS to be ready")
}
