// Package fixture provides test fixtures for integration testing. Fixtures can
// be configured from the environment or started via docker containers. Calling
// the same fixture startup function will return the same instance of the
// fixture.
//
// Once done with all fixtures, the Cleanup function can be called to stop and
// remove any running fixtures.
package fixture

import (
	"context"
	"sync"

	"github.com/docker/docker/client"
)

type fixture struct {
	envVarName        string
	dockerImage       string
	dockerContainerID string
	data              interface{}
}

func (f *fixture) shutdown(ctx context.Context) {
	if f.dockerContainerID != "" {
		if err := stopDockerContainer(ctx, f.dockerContainerID); err != nil {
			logf("failed to stop container %s: %s\n", f.dockerContainerID, err)
		}
	}
}

// fixtures holds a pointer to each fixture so subsequent StartXYZ fixture
// calls can return the existing fixture if any.
var fixtures map[string]*fixture
var fixturesLock sync.Mutex
var dockerClient *client.Client

func init() {
	fixtures = map[string]*fixture{}
	var err error
	dockerClient, err = client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		logf("start docker client: %s\n", err)
	}
}

// Cleanup stops any running test fixtures.
func Cleanup() {
	LogFunc("Cleaning up test fixtures...\n")
	cx, cf := context.WithTimeout(context.Background(), TimeoutCleanup)
	defer cf()
	fixturesLock.Lock()
	for k, fix := range fixtures {
		fix.shutdown(cx)
		delete(fixtures, k)
	}
	fixturesLock.Unlock()
}

// getFixture from the global fixture map.
func getFixture(envVarName string) (fixture *fixture) {
	fixturesLock.Lock()
	if fixtures != nil {
		fixture = fixtures[envVarName]
	}
	fixturesLock.Unlock()
	return fixture
}

// setFixture into the global fixture map, initializing it if needed.
func setFixture(f *fixture) {
	fixturesLock.Lock()
	if fixtures == nil {
		fixtures = map[string]*fixture{
			f.envVarName: f,
		}
	} else {
		fixtures[f.envVarName] = f
	}
	fixturesLock.Unlock()
}
